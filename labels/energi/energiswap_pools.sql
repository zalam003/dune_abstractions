-- Energiswap
nrg_first_token AS (
SELECT
    pair AS pool_address,
    t.symbol as token_0_symbol
FROM nrg."Factory_evt_PairCreated" dex
INNER JOIN erc20.tokens t ON dex."token0" = t.contract_address
WHERE t.symbol IS NOT NULL
),
nrg_second_token AS (
SELECT
    pair AS pool_address,
    t.symbol as token_1_symbol
FROM nrg."Factory_evt_PairCreated" dex
INNER JOIN erc20.tokens t ON dex."token1" = t.contract_address
WHERE t.symbol IS NOT NULL
)

-- Main Query
SELECT
    f.pool_address AS address,
    LOWER(CONCAT('Energiswap LP ', token_0_symbol, ' - ', token_1_symbol)) AS label,
    'lp_pool_name' AS type,
    'masquot' AS author
FROM nrg_first_token f
INNER JOIN nrg_second_token s ON f.pool_address = s.pool_address
