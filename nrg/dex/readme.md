### DEX

- **dex.trades**: Creates the dex.trades schema, intended to match the table in the nrg schema. 

Since we do not yet have price feeds for Optimism tokens, these tables pull USD amounts from **prices.approx_prices_from_dex_data**.

#### Insert Queries

- **dex.insert_energiswap.sql**: Inserts Energiswap data to the dex.trades table.


